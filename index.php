<?php get_header(); ?>
    
    <section class="cd-section posts clear">
	    <h4>Popular Products</h4>
	    <div class="third">
		    <h5>Category</h5> 
		    <div class="title">
			    <h3>Yr home storage solution</h3>
	    	</div>
	    </div>
	    <div class="third">
		    <h5>Category</h5> 
		    <div class="title">
		    	<h3>Store your seasonal stuff</h3>
		    </div>
	    </div>
	    <div class="third">
		    <h5>Category</h5>
		    <div class="title">
		    	<h3>Increase your home’s value</h3> 
		    </div>
	    </div>
    </section>
    
    <section class="cd-section clear">
	    <h4>Services</h4>
    	<div class="category-button shadow">
	    	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/image-digital.jpg" alt="Digital Printing" />
	    	<h4>Category</h4>
	    	<div class="title">
		    	<h2>Digital Printing</h2>
				<a href="#">View Services</a>
	    	</div>
    	</div>
    	<div class="category-button shadow">
	    	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/image-signage.jpg" alt="Digital Printing" />
	    	<h4>Category</h4>
	    	<div class="title">
		    	<h2>Signage</h2>
		    	<a href="#">View Services</a>
	    	</div>
    	</div>
    	<div class="category-button shadow">
	    	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/image-textiles.jpg" alt="Digital Printing" />
	    	<h4>Category</h4>
	    	<div class="title">
		    	<h2>Textiles</h2>
		    	<a href="#">View Services</a>
	    	</div>
    	</div>
    </section>
    
    <section class="cd-section clear">
		<div class="half">
			<h4>About Us</h4>
		    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Nullam quis risus eget urna mollis ornare vel eu leo. Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
		    <a href="#" class="button left">Contact Us</a>
		</div>
    </section>
    
    
    <section class="cd-section clear projects">
		<div class="category-button shadow">
			<div class="overlay"></div>
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/image-project.jpg" alt="Digital Printing" />
			<div class="title">
				<h2>Jagermeister Popup Stand</h2>
				<a href="#">Read More</a>
			</div>
			<h4>Projects</h4>
		</div>
    </section>
    
    <section class="cd-section lead clear">
		<h2>Heard enough? <br>Get in touch with Torque Digital</h2>
		<a href="<?php echo home_url(); ?>/about-us/" class="button">Contact Us</a>
    </section>
   
    
<?php get_footer(); ?>