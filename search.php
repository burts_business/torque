<?php get_header(); ?>

	<section id="post-<?php the_ID(); ?>" class="searchy-baby cd-section clear main intro-section">
	
		<h2><?php _e('Search Results for', 'FoundationPress'); ?> '<?php echo get_search_query(); ?>'</h2>
	
	<?php if ( have_posts() ) : ?>
	
		<?php while ( have_posts() ) : the_post(); ?>
			<div class="searchy-baby">
				<div class="third">
				
				<a href="<?php the_permalink(); ?>">
				    <div class="title">
						<h3><?php the_title(); ?></h3>
						<?php the_field('additional_info');?>
						<a href="<?php the_permalink(); ?>" class="button">Read More</a>
				    </div>
				</a>
		    	
		    </div>
			</div>

		
		<?php endwhile; ?>
		
		<?php else : ?>
			<div class="searchy-baby">
				<h3><?php _e('Sorry, we could not find', 'FoundationPress'); ?> '<?php echo get_search_query(); ?>'</h3>
				<p>Check your spelling or try something else...</p>	
			</div>
			
		    	
		    </div>
			</div>
		
	<?php endif;?>


	</section>
		
<?php get_footer(); ?>