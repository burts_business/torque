<?php /* Template Name: Other */
get_header(); ?>

	<?php if (is_page('About Us')) {?>
	
		<div class="post-content clear">
		<?php if (have_posts()) : ?>
		    <?php while (have_posts()) : the_post(); ?> 
		    <section id="post-<?php the_ID(); ?>" class="cd-section clear main "> 
			    <?php the_title('<h4>', '</h4>');?>
			    <h1>Who is Torque Digital?</h1>
		        <div class="half left tupperware"> 
					<?php the_content(); ?>
		        </div>
		        <div class="half left tupperware">
		        <?php $image = get_field('main_image'); 
			        if( !empty($image) ): ?>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					<?php endif; ?>
		        </div>
				 <?php $image = get_field('third_image'); 
			        if( !empty($image) ): ?>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					<?php endif; ?>

		        <div class="clear"></div>
		       
		        <div class="half left tupperware">
			        <?php $image = get_field('secondary_image'); 
			        if( !empty($image) ): ?>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					<?php endif; ?>
		        </div>
		         <div class="half left tupperware">
			        <?php the_field('secondary');?>
		        </div>
		    </section>
		    <?php endwhile; ?>
		<?php endif; ?>
		</div>
		
	
	<section class="cd-section lead clear">
		<h2>Heard enough? <br>Get in touch with Torque Digital</h2>
		<a href="<?php echo home_url(); ?>/contact-us/" class="button">Contact Us</a>
    </section>

	
	
	<?php } elseif ( is_page('Contact Us') ) {?>
		
		<div id="contact" class="post-content clear">
		<?php if (have_posts()) : ?>
		    <?php while (have_posts()) : the_post(); ?> 
		    <section id="post-<?php the_ID(); ?>" class="cd-section clear main "> 
			    <?php the_title('<h1>', '</h1>');?>
			    <h2><?php the_field('sub_heading');?></h2>
		        <div class="two-thirds left tupperware"> 
					<?php the_content(); ?>
		        </div>
		         <div class="third left tupperware">
			        <?php the_field('secondary');?>
		        </div>
		       
		        <div class="clear"></div>
				
		    </section>
		    <div id="map">
		    </div>
		    <!--<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3192.105990507078!2d174.63136809999997!3d-36.86388174999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sUnit+7%2C+18+Moselle+Avenue%2C+Henderson%2C+Auckland!5e0!3m2!1sen!2snz!4v1438229215019" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>-->
		    <?php endwhile; ?>
		<?php endif; ?>
		</div>

		
	<?php }?>
		
	
					
<?php get_footer(); ?>