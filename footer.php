			<footer class="cd-section clear">
				<div class="row">
					<img class="footer-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/footer-logo.svg" alt-"footer logo" />
				</div>
		        
		        <div class="clear">
			        <div class="third left">
				        <h3>Torque Digital</h3>
				        <div class="search">
							<form method="get" id="searchform" action="<?php bloginfo('home'); ?>/">
								<div>
									<input placeholder="What are you looking for?" type="text" value="<?php echo wp_specialchars($s, 1); ?>" name="s" id="s" />
									<input class="submit" type="image" name="submit" alt="submit" src="<?php bloginfo('stylesheet_directory'); ?>/images/search.svg" alt="search" title="Search" />

								</div>
							</form>
						</div>
			        </div>
			        <div class="third right">
						<a href="tel:+64 9 8375899"> Phone: +64 9 8375899</a>
				        <a href="mailto:sales@torquedigital.co.nz">Email: sales@torquedigital.co.nz</a>
				        <a href="#">Address:  Unit 7, 18 Moselle Avenue, Henderson, Auckland</a>
				    </div>
		        </div>
		        <div class="clear"></div>
		        <div class="third left copyright">
			        <p>&#169; Copyright Torque Digital <?php echo date("Y") ?></p>
		        </div>
		        <div class="third right copyright">
					<a class="blink" href="http://blinkltd.co.nz/" target="_blank">
						handmade by blink.
					</a>
			    </div>

		      

			</footer>		        

			
		</div><!--main content-->

	<a class="exit-off-canvas"></a>

  </div><!--innnerwrap-->
</div><!--off canvas wrap-->


	
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300' rel='stylesheet' type='text/css'>
	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script> 
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/infogrid.js"></script>
	<script>
	  
  	$(document).ready(
    function(){
        $(".menu").click(function () {
            $(".mob-nav").toggle();
        });
    });
    
    $(document).ready(
    function(){
        $(".mob-nav").click(function () {
            $(".mob-nav").hide();
        });
    });
   
	$(document).ready(
    function(){
        $(".close").click(function () {
            $(".mob-nav").hide();
        });
    });


	</script>
	
	<script src="//cdn.rawgit.com/noelboss/featherlight/1.2.3/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
	 <script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')</script>

  <!-- FlexSlider -->
  <script defer src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.flexslider.js"></script>

  <script type="text/javascript">
    $(function(){
      SyntaxHighlighter.all();
    });
    $(window).load(function(){
      $('.flexslider').flexslider({
        animation: "slide",
        slideshow: true, 
        controlNav: "thumbnails",
        slideshowSpeed: 4000,           //Integer: Set the speed of the slideshow cycling, in milliseconds
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
  </script>
  
  
 

	<script>
	 	$(function() {
		  $('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		      var target = $(this.hash);
		      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		      if (target.length) {
		        $('html,body').animate({
		          scrollTop: target.offset().top
		        }, 1000);
		        return false;
		      }
		    }
		  });
		});
		
		

	var fixed = false;
		$(document).scroll(function() {
		    if( $(window).scrollTop() >= 250 ) {
		        if( !fixed ) {
		           fixed = true;
		            $('.header').css({	top: '100px' });
		            
		            
		
		        }
		    } else {
		        if( fixed ) {
		            fixed = false; 
		            $('.header').css({	top: '0px' });
		            $('.mob-nav').css({	display: 'none' });
		        }
		    }
		});
		
	
	</script>
	<style type="text/css">
            /* Set a size for our map container, the Google Map will take up 100% of this container */
            #map {
                width: 100%;
                height: 500px;
            }
        </style>
        
        <!-- 
            You need to include this script tag on any page that has a Google Map.

            The following script tag will work when opening this example locally on your computer.
            But if you use this on a localhost server or a live website you will need to include an API key. 
            Sign up for one here (it's free for small usage): 
                https://developers.google.com/maps/documentation/javascript/tutorial#api_key

            After you sign up, use the following script tag with YOUR_GOOGLE_API_KEY replaced with your actual key.-->
                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAgDW4DLjTAwPAVHXW-2f8wF-16Uts0nW0&sensor=false"></script>
       
        <script type="text/javascript">
            // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);
        
            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 17,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(-36.863647, 174.631768), // New York

                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]},{"featureType":"administrative.country","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]}]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(-36.863647, 174.631768),
                    map: map,
                    title: 'Torque Digital'
                });
            }
        </script>


  
<?php wp_footer(); ?>


</body>
</html>