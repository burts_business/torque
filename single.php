<?php get_header(); ?>

<div class="post-content clear">
	<?php if (have_posts()) : ?>
	    <?php while (have_posts()) : the_post(); ?> 
	    <section id="post-<?php the_ID(); ?>" class="cd-section clear main "> 
		    <!--<h4>Digital Printing</h4>-->
		    <?php the_title('<h1>', '</h1>');?>
	        <div class="sixty left"> 
		    <?php 
				$images = get_field('gallery');
				
				if( $images ): ?>
				    <div id="main-slider" class="flexslider">
						<ul class="slides">
				            <?php foreach( $images as $image ): ?>
				                <li data-thumb="<?php echo $image ['sizes']['thumbnail']; ?>">
				                    <img src="<?php echo $image['sizes']['gallery-image']; ?>" alt="<?php echo $image['alt']; ?>" />
				                </li>
				            <?php endforeach; ?>
				        </ul>
				    </div>
				<?php endif; ?>
				


	        </div>
	        <div class="forty left">
		        <?php the_content();?>
		        <h3>Product Overview</h3>
		        <?php the_field('additional_info');?>
				<a class="button left" href="mailto:sales@torquedigital.co.nz?subject=Product Enquiry">Enquire Now</a>
	        </div>
	    </section>
	    <?php endwhile; ?>
	<?php endif; ?>

</div>
	
		
<?php get_footer(); ?>