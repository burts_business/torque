<?php /* Template Name: Main */
get_header(); ?>

	<?php if (is_page('Digital Printing')) {?>
	
	<section id="post-<?php the_ID(); ?>" class="cd-section clear main intro-section">
		
		<div class="category-button shadow heading">
	    	<img src="<?php the_field('main_image'); ?>" alt="image-<?php the_title(); ?>" />
	    	<div class="title">
		    	<?php the_title('<h1>', '</h1>' );?>
				<p class="sub-title"><?php the_field('sub_title'); ?></p>
	    	</div>
    	</div>
			
		<div class="lead clear">
			<div id="infogrid">
				<div id="page-wrap">
					
					<div class="info-col">
						<?php $args = array(  'category__and' => array(5,16),);
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post(); ?>
							
							<dl>
								<dt id="starter">
									
									<h3><?php the_title(); ?></h3>
								</dt>
								<dd>
									<?php the_field('additional_info');?>
									<a href="<?php the_permalink(); ?>" class="button">Read More</a>
									<div class="clear"></div>
									<?php								 
									$images = get_field('gallery');
									$max = 3;
									$i = 0;
									
									if( $images ): ?>
									            <?php foreach( $images as $image ): $i++; ?>
									            	<?php if( $i > $max){ break; } ?>
									                 <a class="gallery-image" href="<?php echo $image['url']; ?>" data-featherlight="image">
									                     <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
									                </a>
									            <?php endforeach; ?>

									<?php endif; ?>

								</dd>
							</dl>
						<?php endwhile;  ?>
					</div>
					<div class="info-col">
						<?php $args = array(  'category__and' => array(6,16),);
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post(); ?>
							
							<dl>
								<dt id="starter">
									
									<h3><?php the_title(); ?></h3>
								</dt>
								<dd>
									<?php the_field('additional_info');?>
									<a href="<?php the_permalink(); ?>" class="button">Read More</a>
									<div class="clear"></div>
									<?php								 
									$images = get_field('gallery');
									$max = 3;
									$i = 0;
									
									if( $images ): ?>
									            <?php foreach( $images as $image ): $i++; ?>
									            	<?php if( $i > $max){ break; } ?>
									                 <a class="gallery-image" href="<?php echo $image['url']; ?>" data-featherlight="image">
									                     <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
									                </a>
									            <?php endforeach; ?>

									<?php endif; ?>

								</dd>
							</dl>

						<?php endwhile;  ?>
					</div>
	
					<div class="info-col">
						<?php $args = array(  'category__and' => array(7,16),);
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post(); ?>
							
							<dl>
								<dt id="starter">
									
									<h3><?php the_title(); ?></h3>
								</dt>
								<dd>
									<?php the_field('additional_info');?>
									<a href="<?php the_permalink(); ?>" class="button">Read More</a>
									<div class="clear"></div>
									<?php								 
									$images = get_field('gallery');
									$max = 3;
									$i = 0;
									
									if( $images ): ?>
									            <?php foreach( $images as $image ): $i++; ?>
									            	<?php if( $i > $max){ break; } ?>
									                 <a class="gallery-image" href="<?php echo $image['url']; ?>" data-featherlight="image">
									                     <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
									                </a>
									            <?php endforeach; ?>

									<?php endif; ?>

								</dd>
							</dl>
						<?php endwhile;  ?>
					</div>

				</div>
			</div>
	
		</div>	
				
	</section>
	
	<section class="cd-section clear">
	    <h4>Services</h4>
   
    	<div class="category-button shadow">
	    	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/signage.jpg" alt="Digital Printing" />
	    	<div class="title">
		    	<h2>Signage</h2>
		    	<a href="<?php echo home_url(); ?>/signage/">View Services</a>
	    	</div>
    	</div>
    	<div class="category-button shadow">
	    	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/textiles.jpg" alt="Digital Printing" />
	    	<div class="title">
		    	<h2>Textiles</h2>
		    	<a href="<?php echo home_url(); ?>/textiles/">View Services</a>
	    	</div>
    	</div>
    </section>
	
	<?php } elseif ( is_page('Textiles') ) {?>
		
	<section id="post-<?php the_ID(); ?>" class="cd-section clear main intro-section">
		
		<div class="category-button shadow heading">
	    	<img src="<?php the_field('main_image'); ?>" alt="image-<?php the_title(); ?>" />
	    	<div class="title">
		    	<?php the_title('<h1>', '</h1>' );?>
				<p class="sub-title"><?php the_field('sub_title'); ?></p>
	    	</div>
    	</div>

			
		<div class="lead clear">
			<div id="infogrid">
				<div id="page-wrap">
					
					<div class="info-col">
						<?php $args = array(  'category__and' => array(5,75),);
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post(); ?>
							
							<dl>
								<dt id="starter">

									<h3><?php the_title(); ?></h3>
								</dt>
								<dd>
									<?php the_field('additional_info');?>
									<a href="<?php the_permalink(); ?>" class="button">Read More</a>
									<div class="clear"></div>
									<?php								 
									$images = get_field('gallery');
									$max = 3;
									$i = 0;
									
									if( $images ): ?>
									            <?php foreach( $images as $image ): $i++; ?>
									            	<?php if( $i > $max){ break; } ?>
									                 <a class="gallery-image" href="<?php echo $image['url']; ?>" data-featherlight="image">
									                     <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
									                </a>
									            <?php endforeach; ?>

									<?php endif; ?>

								</dd>
							</dl>
						<?php endwhile;  ?>
					</div>
					<div class="info-col">
						<?php $args = array(  'category__and' => array(6,75),);
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post(); ?>
							
							<dl>
								<dt id="starter">

									<h3><?php the_title(); ?></h3>
								</dt>
								<dd>
									<?php the_field('additional_info');?>
									<a href="<?php the_permalink(); ?>" class="button">Read More</a>
									<div class="clear"></div>
									<?php								 
									$images = get_field('gallery');
									$max = 3;
									$i = 0;
									
									if( $images ): ?>
									            <?php foreach( $images as $image ): $i++; ?>
									            	<?php if( $i > $max){ break; } ?>
									                 <a class="gallery-image" href="<?php echo $image['url']; ?>" data-featherlight="image">
									                     <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
									                </a>
									            <?php endforeach; ?>

									<?php endif; ?>

								</dd>
							</dl>
						<?php endwhile;  ?>
					</div>
	
					<div class="info-col">
						<?php $args = array(  'category__and' => array(7,75),);
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post(); ?>
							
							<dl>
								<dt id="starter">

									<h3><?php the_title(); ?></h3>
								</dt>
								<dd>
									<?php the_field('additional_info');?>
									<a href="<?php the_permalink(); ?>" class="button">Read More</a>
									<div class="clear"></div>
									<?php								 
									$images = get_field('gallery');
									$max = 3;
									$i = 0;
									
									if( $images ): ?>
									            <?php foreach( $images as $image ): $i++; ?>
									            	<?php if( $i > $max){ break; } ?>
									                 <a class="gallery-image" href="<?php echo $image['url']; ?>" data-featherlight="image">
									                     <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
									                </a>
									            <?php endforeach; ?>

									<?php endif; ?>

								</dd>
							</dl>

						<?php endwhile;  ?>
					</div>
					

				</div>
			</div>
	
		</div>	
				
	</section>
	
	<section class="cd-section clear">
	    <h4>Services</h4>
		
		<div class="category-button shadow">
	    	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/digital-print.jpg" alt="Digital Printing" />
	    	<h4>Category</h4>
	    	<div class="title">
		    	<h2>Digital Printing</h2>
				<a href="<?php echo home_url(); ?>/digital-printing/">View Services</a>
	    	</div>
    	</div>
    	
    	<div class="category-button shadow">
	    	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/signage.jpg" alt="Digital Printing" />
	    	<div class="title">
		    	<h2>Signage</h2>
		    	<a href="<?php echo home_url(); ?>/signage/">View Services</a>
	    	</div>
    	</div>
    
    </section>	
    
	<?php } elseif ( is_page('Signage') ) {?>
	
	<section id="post-<?php the_ID(); ?>" class="cd-section clear main intro-section">
		
		<div class="category-button shadow heading">
	    	<img src="<?php the_field('main_image'); ?>" alt="image-<?php the_title(); ?>" />
	    	<div class="title">
		    	<?php the_title('<h1>', '</h1>' );?>
				<p class="sub-title"><?php the_field('sub_title'); ?></p>
	    	</div>
    	</div>

			
		<div class="lead clear">
			<div id="infogrid">
				<div id="page-wrap">
					
					<div class="info-col">
						<?php $args = array(  'category__and' => array(5,52),);
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post(); ?>
							
							<dl>
								<dt id="starter">
 
									<h3><?php the_title(); ?></h3>
								</dt>
								<dd>
									<?php the_field('additional_info');?>
									<a href="<?php the_permalink(); ?>" class="button">Read More</a>
									<div class="clear"></div>
									<?php								 
									$images = get_field('gallery');
									$max = 3;
									$i = 0;
									
									if( $images ): ?>
									            <?php foreach( $images as $image ): $i++; ?>
									            	<?php if( $i > $max){ break; } ?>
									                 <a class="gallery-image" href="<?php echo $image['url']; ?>" data-featherlight="image">
									                     <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
									                </a>
									            <?php endforeach; ?>

									<?php endif; ?>

								</dd>
							</dl>

						<?php endwhile;  ?>
					</div>
					<div class="info-col">
						<?php $args = array(  'category__and' => array(6,52),);
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post(); ?>
							
							<dl>
								<dt id="starter">

									<h3><?php the_title(); ?></h3>
								</dt>
								<dd>
									<?php the_field('additional_info');?>
									<a href="<?php the_permalink(); ?>" class="button">Read More</a>
									<div class="clear"></div>
									<?php								 
									$images = get_field('gallery');
									$max = 3;
									$i = 0;
									
									if( $images ): ?>
									            <?php foreach( $images as $image ): $i++; ?>
									            	<?php if( $i > $max){ break; } ?>
									                 <a class="gallery-image" href="<?php echo $image['url']; ?>" data-featherlight="image">
									                     <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
									                </a>
									            <?php endforeach; ?>

									<?php endif; ?>

								</dd>
							</dl>

						<?php endwhile;  ?>
					</div>
	
					<div class="info-col">
						<?php $args = array(  'category__and' => array(7,52),);
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post(); ?>
							
							<dl>
								<dt id="starter">

									<h3><?php the_title(); ?></h3>
								</dt>
								<dd>
									<?php the_field('additional_info');?>
									<a href="<?php the_permalink(); ?>" class="button">Read More</a>
									<div class="clear"></div>
									<?php								 
									$images = get_field('gallery');
									$max = 3;
									$i = 0;
									
									if( $images ): ?>
									            <?php foreach( $images as $image ): $i++; ?>
									            	<?php if( $i > $max){ break; } ?>
									                 <a class="gallery-image" href="<?php echo $image['url']; ?>" data-featherlight="image">
									                     <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
									                </a>
									            <?php endforeach; ?>

									<?php endif; ?>

								</dd>
							</dl>

						<?php endwhile;  ?>
					</div>

				</div>
			</div>
	
		</div>	
				
	</section>
	
	<section class="cd-section clear">
	    <h4>Services</h4>
		
		<div class="category-button shadow">
	    	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/digital-print.jpg" alt="Digital Printing" />
	    	<h4>Category</h4>
	    	<div class="title">
		    	<h2>Digital Printing</h2>
				<a href="<?php echo home_url(); ?>/digital-printing/">View Services</a>
	    	</div>
    	</div>
    	
    	<div class="category-button shadow">
	    	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/textiles.jpg" alt="Digital Printing" />
	    	<div class="title">
		    	<h2>Textiles</h2>
		    	<a href="<?php echo home_url(); ?>/textiles/">View Services</a>
	    	</div>
    	</div>
    </section>
    
    <?php } elseif ( is_page('Projects') ) {?>
	
	<section id="post-<?php the_ID(); ?>" class="cd-section clear main intro-section">
		
		<div class="category-button shadow heading">
	    	<img src="<?php the_field('main_image'); ?>" alt="image-<?php the_title(); ?>" />
	    	<div class="title">
		    	<?php the_title('<h1>', '</h1>' );?>
				<p class="sub-title"><?php the_field('sub_title'); ?></p>
	    	</div>
    	</div>

			
		<div class="lead clear">
			<div id="infogrid">
				<div id="page-wrap">
					
					<div class="info-col">
						<?php $args = array(  'category__and' => array(5,15),);
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post(); ?>
							
							<dl>
								<dt id="starter">
 
									<h3><?php the_title(); ?></h3>
								</dt>
								<dd>
									<?php the_field('additional_info');?>
									<a href="<?php the_permalink(); ?>" class="button">Read More</a>
									<div class="clear"></div>
									<?php								 
									$images = get_field('gallery');
									$max = 3;
									$i = 0;
									
									if( $images ): ?>
									            <?php foreach( $images as $image ): $i++; ?>
									            	<?php if( $i > $max){ break; } ?>
									                 <a class="gallery-image" href="<?php echo $image['url']; ?>" data-featherlight="image">
									                     <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
									                </a>
									            <?php endforeach; ?>

									<?php endif; ?>

								</dd>
							</dl>

						<?php endwhile;  ?>
					</div>
					<div class="info-col">
						<?php $args = array(  'category__and' => array(6,15),);
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post(); ?>
							
							<dl>
								<dt id="starter">

									<h3><?php the_title(); ?></h3>
								</dt>
								<dd>
									<?php the_field('additional_info');?>
									<a href="<?php the_permalink(); ?>" class="button">Read More</a>
									<div class="clear"></div>
									<?php								 
									$images = get_field('gallery');
									$max = 3;
									$i = 0;
									
									if( $images ): ?>
									            <?php foreach( $images as $image ): $i++; ?>
									            	<?php if( $i > $max){ break; } ?>
									                 <a class="gallery-image" href="<?php echo $image['url']; ?>" data-featherlight="image">
									                     <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
									                </a>
									            <?php endforeach; ?>

									<?php endif; ?>

								</dd>
							</dl>
						<?php endwhile;  ?>
					</div>
	
					<div class="info-col">
						<?php $args = array(  'category__and' => array(7,15),);
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post(); ?>
							
							<dl>
								<dt id="starter">
									 
									<h3><?php the_title(); ?></h3>
								</dt>
								<dd>
									<?php the_field('additional_info');?>
									<a href="<?php the_permalink(); ?>" class="button">Read More</a>
									<div class="clear"></div>
									<?php								 
									$images = get_field('gallery');
									$max = 3;
									$i = 0;
									
									if( $images ): ?>
									            <?php foreach( $images as $image ): $i++; ?>
									            	<?php if( $i > $max){ break; } ?>
									                 <a class="gallery-image" href="<?php echo $image['url']; ?>" data-featherlight="image">
									                     <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
									                </a>
									            <?php endforeach; ?>

									<?php endif; ?>

								</dd>
							</dl>

						<?php endwhile;  ?>
					</div>

				</div>
			</div>
	
		</div>	
				
	</section>
	
	<?php } else {?>
		
	<?php }?>
	
	
					
<?php get_footer(); ?>