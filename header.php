<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Torque Digital</title>
    <!-- Thanks to Pure CSS parallax scroll demo #3 by Keith Clark for the Perspective Parallax --> 
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/app.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style.css" type="text/css"/>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/typography.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/core.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/faq.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/custom.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/media.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/style.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/infogrid.css" />
    <link href="//cdn.rawgit.com/noelboss/featherlight/1.3.2/release/featherlight.min.css" type="text/css" rel="stylesheet" title="Featherlight Styles" />
    
	
 	
    
	<script src="//use.typekit.net/bwn3dmz.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>

   	
   <link rel="icon" href="<?php bloginfo('siteurl'); ?>/favicon.ico" type="image/x-icon" />
   <link rel="shortcut icon" href="<?php bloginfo('siteurl'); ?>/favicon.ico" type="image/x-icon" />

   <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/flexslider.css" type="text/css" media="screen" />
	
		
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>

  <div class="off-canvas-wrap">
	 <div class="inner-wrap"> 
		 
		<header id="header" class="header scroll">				
			<nav>
				<a href="<?php echo home_url(); ?>"><img class="home-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.svg" alt="Attic Installations" title="Attic Installations" /></a>
						
				<ul class="right desktop">
					<li><a href="<?php echo home_url(); ?>">Home</a></li>
					<li><a href="<?php echo home_url(); ?>/digital-printing/">Digital Printing</a></li>
					<li><a href="<?php echo home_url(); ?>/signage/">Signage</a></li>
					<li><a href="<?php echo home_url(); ?>/textiles/">Textiles</a></li>
					<li><a href="<?php echo home_url(); ?>/projects/">Projects</a></li>
					<li><a href="<?php echo home_url(); ?>/about-us/">About Us</a></li>
					<li><a href="<?php echo home_url(); ?>/contact-us/">Contact Us</a></li>
					
					
				</ul>			        	
									
			</nav> 	

    		<nav>
				<ul class="mob-nav">
					<li><a href="<?php echo home_url(); ?>">Home</a></li>
					<li><a href="<?php echo home_url(); ?>/digital-printing/">Digital Printing</a></li>
					<li><a href="<?php echo home_url(); ?>/signage/">Signage</a></li>
					<li><a href="<?php echo home_url(); ?>/textiles/">Textiles</a></li>
					<li><a href="<?php echo home_url(); ?>/projects/">Projects</a></li>
					<li><a href="<?php echo home_url(); ?>/about-us/">About Us</a></li>
					<li><a href="<?php echo home_url(); ?>/contact-us/">Contact Us</a></li>
					
					
				</ul>
			</nav>				        	
			<div class="mobile right mob-logo">
				<img class="menu" src="<?php bloginfo('stylesheet_directory'); ?>/images/menu-white.svg" alt="menu" />
			</div>			
		</header>


					
		<div id="main-content" class="clear" role="document">
			
			<?php if ( is_front_page() ){ ?>
		
				
				<section id="intro" class="clear">
					<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 120%;
					margin-left: -5%; width:110%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 110%; }</style>
					<div class='embed-container'><iframe frameborder="0" scrolling="no" seamless="seamless" webkitallowfullscreen="webkitAllowFullScreen" mozallowfullscreen="mozallowfullscreen" allowfullscreen="allowfullscreen" id="okplayer" src="http://player.vimeo.com/video/134781711?api=1&amp;js_api=1&amp;title=0&amp;byline=0&amp;portrait=0&amp;playbar=0&amp;player_id=okplayer&amp;loop=1&amp;autoplay=1"></iframe>
					</div>

					<section class="home-content">
						<div class="gradient"></div>
						<div class="overlay"></div>
						<header class="header-home clear">	
				        	<nav class="desktop">
				        		<ul>
									<li class="left-menu"><a href="<?php echo home_url(); ?>/digital-printing/">Digital Printing</a></li>
									<li class="left-menu"><a href="<?php echo home_url(); ?>/signage/">Signage</a></li>
									<li class="left-menu"><a href="<?php echo home_url(); ?>/textiles/">Textiles</a></li>
									<li class="center-menu"><a href="<?php echo home_url(); ?>"><img class="home-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.svg" alt="Torque Digital" title="Torque Digital" /></a></li>
									<li class="right-menu"><a href="<?php echo home_url(); ?>/contact-us/">Contact Us</a></li>
									<li class="right-menu"><a href="<?php echo home_url(); ?>/about-us/">About Us</a></li>
									<li class="right-menu"><a href="<?php echo home_url(); ?>/projects/">Projects</a></li>
								</ul>
				        	</nav>
				        	
				        	<a class="mobile" href="<?php echo home_url(); ?>"><img class="home-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.svg" alt="Torque Digital" title="Torque Digital" /></a>

				        	<div class="mobile right mob-logo">
								<img class="menu" src="<?php bloginfo('stylesheet_directory'); ?>/images/menu-white.svg" alt="menu" />
							</div>				
							<nav>
								<ul class="mob-nav">
									<li><a href="<?php echo home_url(); ?>">Home</a></li>
									<li><a href="<?php echo home_url(); ?>/digital-printing/">Digital Printing</a></li>
									<li><a href="<?php echo home_url(); ?>/signage/">Signage</a></li>
									<li><a href="<?php echo home_url(); ?>/textiles/">Textiles</a></li>
									<li><a href="<?php echo home_url(); ?>/projects/">Projects</a></li>
									<li><a href="<?php echo home_url(); ?>/about-us/">About Us</a></li>
									<li><a href="<?php echo home_url(); ?>/contact-us/">Contact Us</a></li>
								</ul>
							</nav>
					    </header>				
					     
					
					
					<div class="cd-section">
						<h1>The Future of Digital Print, Textiles and Signage</h1>
						<!--<h3 style="color: white;">Giving Kiwi home owners access to the hidden storage in their homes</h3>-->
						<div class="search">
							<form method="get" id="searchform" action="<?php bloginfo('home'); ?>/">
								<div>
									<input placeholder="What are you looking for?" type="text" value="<?php echo wp_specialchars($s, 1); ?>" name="s" id="s" />
									<input class="submit" type="image" name="submit" alt="submit" src="<?php bloginfo('stylesheet_directory'); ?>/images/search.svg" alt="search" title="Search" />

								</div>
							</form>
						</div>
				    </div>
				    
				</section>
				</section>
			
			<?php }
				else 
			{?>
			
			<section>
				<header class="header-home clear">	
				        	<nav class="desktop">
				        		<ul>
									<li class="left-menu"><a href="<?php echo home_url(); ?>/digital-printing/">Digital Printing</a></li>
									<li class="left-menu"><a href="<?php echo home_url(); ?>/signage/">Signage</a></li>
									<li class="left-menu"><a href="<?php echo home_url(); ?>/textiles/">Textiles</a></li>
									<li class="center-menu"><a href="<?php echo home_url(); ?>"><img class="home-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.svg" alt="Torque Digital" title="Torque Digital" /></a></li>
									<li class="right-menu"><a href="<?php echo home_url(); ?>/contact-us/">Contact Us</a></li>
									<li class="right-menu"><a href="<?php echo home_url(); ?>/about-us/">About Us</a></li>
									<li class="right-menu"><a href="<?php echo home_url(); ?>/projects/">Projects</a></li>
								</ul>
								<ul class="sub-menu">
									<li class="left-menu social"><a href="https://www.facebook.com/TorqueDigital"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/facebook.svg" alt="Facebook" title="Facebook" /></a></li>
									<li class="left-menu social"><a href="#"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/instagram.svg" alt="Instagram" title="Instagram" /></a></li>

									<li class="right-menu search-container">
										<div class="search">
											
											<form method="get" id="searchform" action="<?php bloginfo('home'); ?>/">
												<div>
													
													<input class="submit" type="image" name="submit" alt="submit" src="<?php bloginfo('stylesheet_directory'); ?>/images/search.svg" alt="search" title="Search" />
													<input placeholder="What are you looking for?" type="text" value="<?php echo wp_specialchars($s, 1); ?>" name="s" id="s" />
												</div>
											</form>

										</div>
									</li>
								</ul>
				        	</nav>
				        
				        
				   

				        	
				        	<a class="mobile" href="<?php echo home_url(); ?>"><img class="home-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.svg" alt="Torque Digital" title="Torque Digital" /></a>

				        		<nav>
								<ul class="mob-nav">
									<li><a href="<?php echo home_url(); ?>">Home</a></li>
									<li><a href="<?php echo home_url(); ?>/digital-printing/">Digital Printing</a></li>
									<li><a href="<?php echo home_url(); ?>/signage/">Signage</a></li>
									<li><a href="<?php echo home_url(); ?>/textiles/">Textiles</a></li>
									<li><a href="<?php echo home_url(); ?>/projects/">Projects</a></li>
									<li><a href="<?php echo home_url(); ?>/about-us/">About Us</a></li>
									<li><a href="<?php echo home_url(); ?>/contact-us/">Contact Us</a></li>
									
									
								</ul>
							</nav>				        	
							<div class="mobile right mob-logo">
								<img class="menu" src="<?php bloginfo('stylesheet_directory'); ?>/images/menu-white.svg" alt="menu" />
							</div>				
					    </header>				
			     
			</section>
			  			
			<?php }
			?>

			
			
	
			