<?php get_header(); ?>
    
    <section class="cd-section posts clear">
	    <h4>Popular Products</h4>
	    <?php $args = array( 'category_name' => 'popular', 'posts_per_page' => 3 );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post(); ?>
					
		
			<div class="third">
			    <h5>Popular</h5> 
			    <div class="title">
				    <a href="<?php the_permalink(); ?>">
				    <h3><?php the_title(); ?></h3>
				    </a>
		    	</div>
		    </div>

		<?php endwhile; 
		wp_reset_query(); ?>
    
    </section>
    
    <section class="cd-section clear">
	    <h4>Services</h4>
    	<div class="category-button shadow">
	    	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/digital-print.jpg" alt="Digital Printing" />
	    	<h4>Category</h4>
	    	<div class="title">
		    	<h2>Digital Printing</h2>
				<a href="<?php echo home_url(); ?>/digital-printing/">View Services</a>
	    	</div>
	    	
    	</div>
    	<div class="category-button shadow">
	    	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/signage.jpg" alt="Digital Printing" />
	    	<h4>Category</h4>
	    	<div class="title">
		    	<h2>Signage</h2>
		    	<a href="<?php echo home_url(); ?>/signage/">View Services</a>
	    	</div>
    	</div>
    	<div class="category-button shadow">
	    	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/textiles.jpg" alt="Digital Printing" />
	    	<h4>Category</h4>
	    	<div class="title">
		    	<h2>Textiles</h2>
		    	<a href="<?php echo home_url(); ?>/textiles/">View Services</a>
	    	</div>
    	</div>
    </section>
    
    <section class="cd-section clear">
		<div class="half">
			<?php the_content(); ?>
		    <a href="<?php echo home_url(); ?>/contact-us/" class="button left">Contact Us</a>
		</div>
    </section>
    
    
    <section class="cd-section clear projects">
		<div class="category-button shadow">
			<div class="overlay"></div>
			<?php $args = array( 'category_name' => 'project', 'posts_per_page' => 1 );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post(); ?>
					
			<?php if ( has_post_thumbnail() ) {
			the_post_thumbnail();
			} ?>
			<div class="title">
				<h2><?php the_title(); ?></h2>
				<a href="<?php the_permalink(); ?>">Read More</a>
			</div>
			<h4>Projects</h4>
			<?php endwhile; 
				
			wp_reset_query(); ?>

		</div>
    </section>
    
    <section class="cd-section lead clear">
		<h2>Heard enough? <br>Get in touch with Torque Digital</h2>
		<a href="<?php echo home_url(); ?>/contact-us/" class="button">Contact Us</a>
    </section>
   
    
<?php get_footer(); ?>